CREATE TABLE `user` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(50) UNIQUE NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `level` int(11) NOT NULL
);

CREATE TABLE parent(
    Pprenom varchar (50) NOT NULL,
    Pnom varchar (50) NOT NULL,
    telephone varchar(15),
    email varchar(50) references user (email)
);

CREATE TABLE enfant(
    id int(11) AUTO_INCREMENT PRIMARY KEY,
    nomEnf varchar(50),
    prenomEnf varchar(50),
    Pprenom varchar (50) references parent(Pprenom),
    Pnom varchar (50) references parent(Pnom),
    telephone varchar(15) references parent(telephone)
);

CREATE TABLE coordBancaire(
   id int(11) AUTO_INCREMENT PRIMARY KEY,
  `img` BLOB NOT NULL,
  `extension` VARCHAR(25) NOT NULL,
   email varchar(50) references user (email)
 );

CREATE TABLE educateur(
    id int(11) AUTO_INCREMENT PRIMARY KEY,
    nomEduc varchar(50),
    prenomEduc varchar(50),
    nomActivite varchar (50) references activite(nomActivite)
);


CREATE TABLE activite(
    id int(11) AUTO_INCREMENT PRIMARY KEY,
    activite varchar(50) NOT NULL,
    dateInscription date,
    nbplace int(2),
    nomEduc varchar(50) references educateur (nomEduc),
    nomEnf varchar(50) references enfant(nomEnf)
);
