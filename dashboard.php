
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--<meta http-equiv="x-ua-compatible" content="ie=edge">-->

    <title>Children Garden – Dashboard</title>

    <!-- Font Awesome -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,600,300,300italic,700' rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Material Design Bootstrap -->
    <link href="css/materialize.css" rel="stylesheet">

    <!-- Magnific-popup css -->
    <link href="css/magnific-popup.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <!--<link href="css/progressbar.css" rel="stylesheet">-->

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/children_ico.png">


    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <?php include('verificationUser.php'); ?>

</head>

<body data-spy="scroll" data-target=".navbar-desktop">
<!-- Start your project here-->
<!--Navbar-->

<div class='preloader'>
    <div class='loaded'>&nbsp;</div>
</div>

<nav class="navbar navbar-fixed-top navbar-light bg-faded">
    <!--Collapse button-->
    <div class="container">
        <a href="#" data-activates="mobile-menu" class="button-collapse right"><i class="fa fa-bars black-text"></i></a>

        <!--Content for large and medium screens-->
        <div class="navbar-desktop">
            <!--Navbar Brand-->
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt=""/></a>
            <!--Links-->
            <ul class="nav navbar-nav pull-right hidden-md-down text-uppercase">
                <li class="nav-item">
                    <?php echo '<a class="nav-link" href="closeSession.php">déconnexion</a>' ?>
                </li>

            </ul>

        </div>

        <!-- Content for mobile devices-->
        <div class="navbar-mobile">

            <ul class="side-nav" id="mobile-menu">
                <li class="nav-item">
                    <?php echo '<a class="nav-link" href="closeSession.php">déconnexion</a>' ?>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar-->

<br><br><br>

<section id="service" class="service">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main_service_area">
                    <div class="head_title center m-y-3 wow fadeInUp">
                        <?php

                        session_start();
                        include "connexionBDD.php";// on se connecte à la BDD (ne pas oublier de fermer la connexion plus tard)

                        if(isset($_SESSION["email"])) // si la variable $_SESSION["email"] existe on considère que le client est connecté car c'est une variable côtès serveur
                        {
                            echo '<p>Bonjour ' . $_SESSION['email'] . '</p>';
                        }

                        include "closeConnexionBDD.php"; // on ferme la connexion à la BDD
                        ?>
                        <h2>Tableau de bord</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="jumbotron single_service  wow fadeInLeft">
                                <div class="service_icon center">
                                    <i class="fa fa-info-circle m-b-3"></i>

                                </div>
                                <div class="s_service_text text-sm-center text-xs-center">
                                    <h4>Responsable légal</h4>
                                    <p>Lorem ipsum dolor sit amet, conse tetuer adipiscing elit, sed diam nonu my nibh
                                        euismod
                                        tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                </div>

                                <div class="service_btn center">
                                    <a href="registerResponsable.php" class="btn btn-danger waves-effect waves-red">See More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="jumbotron single_service wow fadeInUp">
                                <div class="service_icon center">
                                    <i class="fa fa-child m-b-3"></i>
                                </div>
                                <div class="s_service_text text-sm-center text-xs-center">
                                    <h4>Vos Enfants</h4>
                                    <p>Lorem ipsum dolor sit amet, conse tetuer adipiscing elit, sed diam nonu my nibh
                                        euismod
                                        tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                </div>

                                <div class="service_btn center">
                                    <a href="registerEnfant.php" class="btn btn-danger waves-effect waves-red">See More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="jumbotron single_service wow fadeInRight">
                                <div class="service_icon center">
                                    <i class="fa fa-credit-card-alt m-b-3"></i>
                                </div>
                                <div class="s_service_text text-sm-center  text-xs-center">
                                    <h4>Moyen de paiment</h4>
                                    <p>Lorem ipsum dolor sit amet, conse tetuer adipiscing elit, sed diam nonu my nibh
                                        euismod
                                        tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                </div>

                                <div class="service_btn center">
                                    <a href="registerMoyenPaiment.php" class="btn btn-danger waves-effect waves-red">See More</a>
                                </div>


                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="jumbotron single_service  wow fadeInLeft">
                                    <div class="service_icon center">
                                        <i class="fa fa-users m-b-3"></i>
                                    </div>
                                    <div class="s_service_text text-sm-center text-xs-center">
                                        <h4>Garderie</h4>
                                        <p>Lorem ipsum dolor sit amet, conse tetuer adipiscing elit, sed diam nonu my
                                            nibh euismod
                                            tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                    </div>

                                    <div class="service_btn center">
                                        <a href="#!" class="btn btn-danger waves-effect waves-red">See More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="jumbotron single_service wow fadeInUp">
                                    <div class="service_icon center">
                                        <i class="fa fa-cutlery m-b-3"></i>
                                    </div>
                                    <div class="s_service_text text-sm-center text-xs-center">
                                        <h4>Cantine</h4>
                                        <p>Lorem ipsum dolor sit amet, conse tetuer adipiscing elit, sed diam nonu my
                                            nibh euismod
                                            tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                    </div>

                                    <div class="service_btn center">
                                        <a href="#!" class="btn btn-danger waves-effect waves-red">See More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="jumbotron single_service wow fadeInRight">
                                    <div class="service_icon center">
                                        <i class="fa fa-cubes  m-b-3"></i>

                                    </div>
                                    <div class="s_service_text text-sm-center  text-xs-center">
                                        <h4>Ludothéque</h4>
                                        <p>Lorem ipsum dolor sit amet, conse tetuer adipiscing elit, sed diam nonu my
                                            nibh euismod
                                            tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                    </div>

                                    <div class="service_btn center">
                                        <a href="#!" class="btn btn-danger waves-effect waves-red">See More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
</section> <!-- End of service section -->


<section id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="main_footer_area white-text p-b-3">
                <div class="col-md-3">
                    <div class="single_f_widget p-t-3 wow fadeInUp">
                        <img src="img/logo.png" alt=""/>
                        <div class="single_f_widget_text">
                            <p>It is a long established fact that a reader will be distracted by the readable content of
                                a page when looking at its layout.
                                The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                            <div class="socail_f_widget">
                                <a href="#!"><i class="fa fa-facebook"></i></a>
                                <a href="#!"><i class="fa fa-google-plus"></i></a>
                                <a href="#!"><i class="fa fa-twitter"></i></a>
                                <a href="#!"><i class="fa fa-vimeo"></i></a>
                                <a href="#!"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_f_widget m-t-3 wow fadeInUp">
                        <h4 class="text-lowercase">Some features</h4>
                        <div class="single_f_widget_text f_reatures">
                            <ul>
                                <li><i class="fa fa-check"></i>Lorem ipsum dolor sit amet</li>
                                <li><i class="fa fa-check"></i>Aliquam tincidunt cons ectetuer</li>
                                <li><i class="fa fa-check"></i>Vestibulum auctor dapibus con</li>
                                <li><i class="fa fa-check"></i>Lorem ipsum dolor sit amet auctor dapibus</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single_f_widget m-t-3 wow fadeInUp">
                        <h4 class="text-lowercase">Tags</h4>
                        <div class="single_f_widget_text f_tags">
                            <a href="#!">corporate</a>
                            <a href="#!">agency</a>
                            <a href="#!">portfolio</a>
                            <a href="#!">blog</a>
                            <a href="#!">elegant</a>
                            <a href="#!">professional</a>
                            <a href="#!">business</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_coppyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="copyright_text m-t-2 text-xs-center">
                            <p class="wow zoomIn" data-wow-duration="1s">Made with <i class="fa fa-heart"></i> by <a
                                        target="_black" href="http://bootstrapthemes.co"> Bootstrap Themes</a> 2016. All
                                Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="socail_coppyright text-sm-right m-t-2 text-xs-center wow zoomIn">
                            <a href="#!"><i class="fa fa-facebook"></i></a>
                            <a href="#!"><i class="fa fa-twitter"></i></a>
                            <a href="#!"><i class="fa fa-google-plus"></i></a>
                            <a href="#!"><i class="fa fa-rss"></i></a>
                            <a href="#!"><i class="fa fa-vimeo"></i></a>
                            <a href="#!"><i class="fa fa-pinterest"></i></a>
                            <a href="#!"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<!-- /Start your project here-->


<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="js/tether.min.js"></script>


<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.min.js"></script>

<!-- Wow js -->
<script type="text/javascript" src="js/wow.min.js"></script>

<!-- Mixitup js -->
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>

<!-- Magnific-popup js -->
<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>

<!-- accordion js -->
<script type="text/javascript" src="js/accordion.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/materialize.js"></script>

<script>
    $(".button-collapse").sideNav();
</script>

<!-- wow js active -->
<script type="text/javascript">
    new WOW().init();
</script>

<script type="text/javascript" src="js/plugins.js"></script>
<script type="text/javascript" src="js/main.js"></script>


</body>
</html>
