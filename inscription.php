<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--<meta http-equiv="x-ua-compatible" content="ie=edge">-->

    <title>Children Garden – Inscription</title>

    <!-- Font Awesome -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,600,300,300italic,700' rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Material Design Bootstrap -->
    <link href="css/materialize.css" rel="stylesheet">

    <!-- Magnific-popup css -->
    <link href="css/magnific-popup.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <!--<link href="css/progressbar.css" rel="stylesheet">-->

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/children_ico.png">


    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

</head>
<div class='preloader'>
    <div class='loaded'>&nbsp;</div>
</div>

<nav class="navbar navbar-fixed-top navbar-light bg-faded">
    <!--Collapse button-->
    <div class="container">
        <a href="#" data-activates="mobile-menu" class="button-collapse right"><i class="fa fa-bars black-text"></i></a>

        <!--Content for large and medium screens-->
        <div class="navbar-desktop">
            <!--Navbar Brand-->
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt=""/></a>
            <!--Links-->
            <ul class="nav navbar-nav pull-right hidden-md-down text-uppercase">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="activite.php">Activité</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="garderie.php">Garderie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="accueilcantine.php">Cantine</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ludotheque.php">Ludothéque</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="connexion.php">Connexion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inscription.php">Inscription</a>
                </li>
            </ul>

        </div>

        <!-- Content for mobile devices-->
        <div class="navbar-mobile">

            <ul class="side-nav" id="mobile-menu">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="activite.php">Activité</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="garderie.php">Garderie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="accueilcantine.php">Cantine</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ludotheque.php">Ludothéque</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="connexion.php">Connexion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inscription.php">Inscription</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<!--/.Navbar-->


<br> <br> <br>
<section id="Inscription" class=" Inscription">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main_service_area">
                    <div class="head_title center m-y-3 wow fadeInUp">
                        <h2>Inscription</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-pull-6">
                            <div class="jumbotron single_service  wow fadeInLeft">
                                <div class="service_icon center">
                                    <i class="fa fa-expeditedssl m-b-2"></i>
                                </div>
                                <div class="s_service_text text-sm-center text-xs-center">
                                    <?php
                                    session_start();
                                    include "connexionBDD.php";

                                    if (!isset($_POST["send"])) {


                                        echo '	<form method="post" id="formulaire" action="inscription.php" accept-charset="UTF-8" enctype="multipart/form-data">

					                <input type="text" name="send" hidden/>
					
					<p> Vous êtes: &nbsp; &nbsp;
					<input type="checkbox" id="parent" name="cklevel[]" value="1">
                    <label for="parent">Parent</label> &nbsp; &nbsp; &nbsp; &nbsp;
                    
                    <input type="checkbox" id="professionnel" name="cklevel[]" value="0">
                    <label for="professionnel">Professionnel</label>
                    </p>
		   			<p>Email : <input type="email" id="saisieEmail" name="email" /></p>
		   			<p>Password : <input type="password" id="saisiePassword" name="password" /></p>
		   			<p>Confirmer password : <input type="password" id="saisiePassword2" name="password2" /></p>
		   			
		   		   <p><input type="submit" id="submit" value="Inscription" /></p>
				</form>';


                                    } else {
                                        if (isset($_POST["email"]) && $_POST["email"] != "") {
                                            if (isset($_POST["password"]) && $_POST["password"] != "") {
                                                if (isset($_POST["password2"]) && $_POST["password"] != "") {
                                                    $email = htmlspecialchars($_POST["email"]);
                                                    $password = htmlspecialchars($_POST["password"]);
                                                    $password2 = htmlspecialchars($_POST["password2"]);
                                                    foreach ($_POST ['cklevel'] as $level) ;
                                                    {
                                                        $level = $level;
                                                    }

                                                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                                                        if ($password == $password2) {
                                                            $password = md5($password);


                                                            $req = $bdd->prepare('SELECT id FROM user;');
                                                            $req->execute(array());
                                                            $count = $req->rowCount();
                                                            $req->closeCursor();


                                                            $req = $bdd->prepare('SELECT id FROM user WHERE email = ?;');
                                                            $req->execute(array($email));
                                                            $count = $req->rowCount();
                                                            $req->closeCursor();

                                                            if ($count == 0) {
                                                                $req = $bdd->prepare('INSERT INTO user (email, mdp, level) VALUES (?, ?, ?);');
                                                                $req->execute(array($email, $password, $level));
                                                                $req->closeCursor();

                                                                session_regenerate_id();
                                                                $_SESSION["email"] = $email;
                                                                $_SESSION["password"] = $password;
                                                                $_SESSION["level"] = $defaultLevel;

                                                                $req = $bdd->prepare('SELECT id FROM user WHERE email = ?;');
                                                                $req->execute(array($email));
                                                                $count = $req->rowCount();
                                                                $req->closeCursor();

                                                                if ($count != 0) {
                                                                    $result = "Inscription réussie";
                                                                } else {
                                                                    $result = "Echec de l'inscription : erreur interne !";
                                                                }
                                                            } else {
                                                                $result = "Cet Email existe dèja !";
                                                            }
                                                        } else {
                                                            $result = "Les Mots de passe sont différents !";
                                                        }
                                                    } else {
                                                        $result = "Email invalide !";
                                                    }
                                                } else {
                                                    $result = "Mot de passe de vérification non saisi !";
                                                }
                                            } else {
                                                $result = "Mot de passe non saisi !";
                                            }
                                        } else {
                                            $result = "Email non saisi !";
                                        }


                                        if ($result == "Inscription réussie") {
                                            echo '<p>' . $result . '</p>';
                                            header("Refresh:4; url=connexion.php", true, 303);
                                        } else {
                                            echo '<p>' . $result . '</p>';
                                            header("Refresh:4; url=inscription.php", true, 303);
                                        }

                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="main_footer_area white-text p-b-3">
                <div class="col-md-3">
                    <div class="single_f_widget p-t-3 wow fadeInUp">
                        <img src="img/logo.png" alt=""/>
                        <div class="single_f_widget_text">
                            <p>It is a long established fact that a reader will be distracted by the readable content of
                                a page when looking at its layout.
                                The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                            <div class="socail_f_widget">
                                <a href="#!"><i class="fa fa-facebook"></i></a>
                                <a href="#!"><i class="fa fa-google-plus"></i></a>
                                <a href="#!"><i class="fa fa-twitter"></i></a>
                                <a href="#!"><i class="fa fa-vimeo"></i></a>
                                <a href="#!"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_f_widget m-t-3 wow fadeInUp">
                        <h4 class="text-lowercase">Some features</h4>
                        <div class="single_f_widget_text f_reatures">
                            <ul>
                                <li><i class="fa fa-check"></i>Lorem ipsum dolor sit amet</li>
                                <li><i class="fa fa-check"></i>Aliquam tincidunt cons ectetuer</li>
                                <li><i class="fa fa-check"></i>Vestibulum auctor dapibus con</li>
                                <li><i class="fa fa-check"></i>Lorem ipsum dolor sit amet auctor dapibus</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_f_widget m-t-3 wow fadeInUp">
                        <h4 class="text-lowercase">Tags</h4>
                        <div class="single_f_widget_text f_tags">
                            <a href="#!">corporate</a>
                            <a href="#!">agency</a>
                            <a href="#!">portfolio</a>
                            <a href="#!">blog</a>
                            <a href="#!">elegant</a>
                            <a href="#!">professional</a>
                            <a href="#!">business</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_f_widget m-t-3 wow fadeInUp">
                        <h4 class="text-lowercase">Flicker Posts</h4>
                        <div class="single_f_widget_text f_flicker">
                            <img src="img/flipcker1.jpg" alt=""/>
                            <img src="img/flipcker2.jpg" alt=""/>
                            <img src="img/flipcker3.jpg" alt=""/>
                            <img src="img/flipcker4.jpg" alt=""/>
                            <img src="img/flipcker3.jpg" alt=""/>
                            <img src="img/flipcker2.jpg" alt=""/>
                            <img src="img/flipcker4.jpg" alt=""/>
                            <img src="img/flipcker1.jpg" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_coppyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="copyright_text m-t-2 text-xs-center">
                        <p class="wow zoomIn" data-wow-duration="1s">Made with <i class="fa fa-heart"></i> by <a
                                    target="_black" href="http://bootstrapthemes.co"> Bootstrap Themes</a> 2016. All
                            Rights Reserved</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="socail_coppyright text-sm-right m-t-2 text-xs-center wow zoomIn">
                        <a href="#!"><i class="fa fa-facebook"></i></a>
                        <a href="#!"><i class="fa fa-twitter"></i></a>
                        <a href="#!"><i class="fa fa-google-plus"></i></a>
                        <a href="#!"><i class="fa fa-rss"></i></a>
                        <a href="#!"><i class="fa fa-vimeo"></i></a>
                        <a href="#!"><i class="fa fa-pinterest"></i></a>
                        <a href="#!"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- /Start your project here-->


<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="js/tether.min.js"></script>


<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.min.js"></script>

<!-- Wow js -->
<script type="text/javascript" src="js/wow.min.js"></script>

<!-- Mixitup js -->
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>

<!-- Magnific-popup js -->
<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>

<!-- accordion js -->
<script type="text/javascript" src="js/accordion.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/materialize.js"></script>

<script>
    $(".button-collapse").sideNav();
</script>

<!-- wow js active -->
<script type="text/javascript">
    new WOW().init();
</script>

<script type="text/javascript" src="js/plugins.js"></script>
<script type="text/javascript" src="js/main.js"></script>


</body>
</html>



