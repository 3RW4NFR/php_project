<!DOCTYPE html>
<html>
	<head>
		<title>Accueil</title>
	</head>

	<body>
		<h1>Accueil</h1>
		<p><a href="connexion.php">Connexion</a></p>
		<p><a href="inscription.php">Inscription</a></p>
		<?php
			session_start();
			include "connexionBDD.php"; // on se connecte à la BDD (ne pas oublier de fermer la connexion plus tard)
        echo '<p>Bonjour ' . $_SESSION['email'] . '</p>';
        $email = $_SESSION['email']; // recuperation du mail dans le cookie
        $req = $bdd->prepare('SELECT Pprenom FROM parent WHERE email = ?;');
        $req->execute(array($email));
        $count = $req->rowCount();
        $req->closeCursor();


        if (isset($_SESSION["email"])) // si la variable $_SESSION["email"] existe on considère que le client est connecté car c'est une variable côtès serveur
            {
                echo '<p><a href="closeSession.php">Deconnexion</a></p>';
                echo '<h2>Vous êtes connecté !</h2>';


                if (isset($_SESSION["level"])) {
                    if ($_SESSION["level"] == 0) {
                        echo '<h3>Vous êtes administrateur !</h3>';
                        header("Refresh:4; url=connexion.php", true, 303);
                    } else {
                        echo '<h3>Vous êtes utilisateur !</h3>';
                    }
                }
            } else {
                echo '<h2>Vous n\'êtes pas connecté !</h2>';

        }
			include "closeConnexionBDD.php"; // on ferme la connexion à la BDD
		?>
	</body>
</html>
