<?php
if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}

if(isset($_SESSION["level"]))
{
    if($_SESSION["level"] != 0)
    {
        header( "Location: index.php");
    }
}
else
{
    header( "Location: index.php");
}
?>
